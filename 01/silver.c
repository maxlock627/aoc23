#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BUFFER_SIZE 1024

int main(int argc, char* argv[]) {
    FILE* file = fopen(argv[1], "r");
    if (!file) {
        printf("Error opening file.\n");
        return 1;
    }
    char buffer[BUFFER_SIZE];
    size_t sum = 0;
    while (fgets(buffer, BUFFER_SIZE, file)) {
        int first_digit = -1;
        int last_digit = -1;
        size_t len = strlen(buffer);
        for (int i = 0; i < len; i++) {
            if (first_digit == -1) {
                if (isdigit(buffer[i])) {
                    first_digit = (int)(buffer[i]-'0');
                    last_digit = (int)(buffer[i]-'0');
                }
            }
            else {
                if (isdigit(buffer[i])) {
                    last_digit = (int)(buffer[i]-'0');
                }
            }
        }
        sum += (10*first_digit)+last_digit;
    }
    printf("sum: %d\n", sum);
    fclose(file);
    return 0;
}
