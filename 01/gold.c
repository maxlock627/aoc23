#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 1024

typedef struct {
    const char* key;
    const char* value;
} Pair;

Pair map[] = {
    {"one", "o1e"},
    {"two", "t2o"},
    {"three", "t3ree"},
    {"four", "f4ur"},
    {"five", "f5ve"},
    {"six", "s6x"},
    {"seven", "s7ven"},
    {"eight", "e8ght"},
    {"nine", "n9ne"},
};

int main(int argc, char* argv[]) {
    FILE* file = fopen(argv[1], "r");
    if (!file) {
        printf("Error opening file.\n");
        return -1;
    }
    char buffer[BUFFER_SIZE];
    size_t sum = 0;
    while (fgets(buffer, BUFFER_SIZE, file)) {
        // printf("%s", buffer);
        for (int i = 0; i < 9; i++) {
            //len of substr
            int len = strlen(map[i].value);
            //pointer to first occurance of substr
            char* ptr;
            //copy of buffer for safe modification
            char* str = buffer;
            //replace all instance of a key with its value
            while (ptr = strstr(str, map[i].key)) {
                strncpy(ptr, map[i].value, len);
            }
        }
        int first_digit = -1;
        int last_digit = -1;
        size_t len = strlen(buffer);
        for (int i = 0; i < len; i++) {
            if (first_digit == -1) {
                if (isdigit(buffer[i])) {
                    first_digit = (int)(buffer[i]-'0');
                    last_digit = (int)(buffer[i]-'0');
                }
            }
            else {
                if (isdigit(buffer[i])) {
                    last_digit = (int)(buffer[i]-'0');
                }
            }
        }
        // printf("%s: %d, %d\n", buffer, first_digit, last_digit);
        sum += (10*first_digit)+last_digit;
    }
    printf("sum: %d\n", sum);
    fclose(file);
    return 0;
}
