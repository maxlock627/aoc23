#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define BUFFER_SIZE 1024

int main(int argc, char* argv[]) {
    FILE *file = fopen(argv[1], "r");
    if (!file) {
        printf("Error opening file.\n");
        return 1;
    }

    char buffer[BUFFER_SIZE];
    int sum = 0;

    const int red_max = 12;
    const int green_max = 13;
    const int blue_max = 14;

    while (fgets(buffer, BUFFER_SIZE, file)) {
        const int len = strlen(buffer);
        for (int i = 0; i < len; i++) {
            switch (buffer[i]) {
            case ':':
            case ';':
            case ',':
            case '\n':
                buffer[i] = ' ';
                break;
            }
        }
        int red_count = 0;
        int green_count = 0;
        int blue_count = 0;

        char* save = NULL;
        char* token = strtok_r(buffer, " ", &save);
        token = strtok_r(NULL, " ", &save);
        const int game_number = atoi(token);
        token = strtok_r(NULL, " ", &save);

        while(token) {
            const int num = atoi(token);
            token = strtok_r(NULL, " ", &save);
            const char* color = token;
            if (strcmp(color, "red") == 0 && num > red_count) {
                red_count = num;
            }
            if (strcmp(color, "green") == 0 && num > green_count) {
                green_count = num;
            }
            if (strcmp(color, "blue") == 0 && num > blue_count) {
                blue_count = num;
            }
            token = strtok_r(NULL, " ", &save);
        }
        sum+=(red_count*green_count*blue_count);
    }
    printf("sum: %d\n", sum);
    fclose(file);
    return 0;
}
